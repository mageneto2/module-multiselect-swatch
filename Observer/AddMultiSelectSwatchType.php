<?php

namespace Pmclain\MultiSelectSwatch\Observer;

class AddMultiSelectSwatchType implements \Magento\Framework\Event\ObserverInterface
{

    protected \Magento\Framework\Module\Manager $moduleManager;

    public function __construct(\Magento\Framework\Module\Manager $moduleManager)
    {
        $this->moduleManager = $moduleManager;
    }

    public function execute(\Magento\Framework\Event\Observer $observer): void
    {
        if (!$this->moduleManager->isOutputEnabled('Pmclain_MultiSelectSwatch')) {
            return;
        }

        /** @var \Magento\Framework\DataObject $response */
        $response = $observer->getEvent()->getResponse();
        $types = $response->getTypes();
        $types[] = [
            'value' => \Pmclain\MultiSelectSwatch\Model\Swatch::SWATCH_MULTISELECT_TYPE_VISUAL_ATTRIBUTE_FRONTEND_INPUT,
            'label' => __('Visual Swatch - Multiselect'),
            'hide_fields' => [
                'is_unique',
                'is_required',
                'frontend_class',
                '_scope',
                '_default_value',
            ],
        ];
        $types[] = [
            'value' => \Pmclain\MultiSelectSwatch\Model\Swatch::SWATCH_MULTISELECT_TYPE_TEXTUAL_ATTRIBUTE_FRONTEND_INPUT,
            'label' => __('Text Swatch - Multiselect'),
            'hide_fields' => [
                'is_unique',
                'is_required',
                'frontend_class',
                '_scope',
                '_default_value',
            ],
        ];

        $response->setTypes($types);
    }

}
