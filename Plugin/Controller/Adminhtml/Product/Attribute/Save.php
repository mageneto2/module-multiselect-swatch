<?php

namespace Pmclain\MultiSelectSwatch\Plugin\Controller\Adminhtml\Product\Attribute;

class Save
{

    /**
     * @param \Magento\Catalog\Controller\Adminhtml\Product\Attribute\Save $subject
     * @param \Magento\Framework\App\RequestInterface $request
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeDispatch(
        \Magento\Catalog\Controller\Adminhtml\Product\Attribute\Save $subject,
        \Magento\Framework\App\RequestInterface $request
    )
    {
        return [$request];
        $data = $request->getPostValue();
        if (isset($data['frontend_input'])) {
            switch ($data['frontend_input']) {
                case \Pmclain\MultiSelectSwatch\Model\Swatch::SWATCH_MULTISELECT_TYPE_VISUAL_ATTRIBUTE_FRONTEND_INPUT:
                    $data[\Magento\Swatches\Model\Swatch::SWATCH_INPUT_TYPE_KEY] = \Magento\Swatches\Model\Swatch::SWATCH_INPUT_TYPE_VISUAL;
                    $data['frontend_input'] = 'multiselect';
                    $request->setPostValue($data);
                    break;
                case \Pmclain\MultiSelectSwatch\Model\Swatch::SWATCH_MULTISELECT_TYPE_TEXTUAL_ATTRIBUTE_FRONTEND_INPUT:
                    $data[\Magento\Swatches\Model\Swatch::SWATCH_INPUT_TYPE_KEY] = \Magento\Swatches\Model\Swatch::SWATCH_INPUT_TYPE_TEXT;
                    $data['use_product_image_for_swatch'] = 0;
                    $data['frontend_input'] = 'multiselect';
                    $request->setPostValue($data);
                    break;
            }
        }

        return [$request];
    }
    
}
