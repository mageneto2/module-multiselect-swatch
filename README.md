# Magento 2 Multiselect Swatches

This module is based on https://packagist.org/packages/starringjane/module-multiselect-swatch and works with PHP ~8.1

### Added Notes
I can confirm this version is working on Magento 2.4.6-p3

## License
Open Software License v3.0
